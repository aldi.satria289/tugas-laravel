@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h5 class="card-title m-0">Pertanyaan</h5>
    </div>
    <div class="card-body">
      <h6 class="card-title"><h3>{{$tanyaan->judul}}</h3></h6>

      <p class="card-text"><p>{{$tanyaan->isi}}</p></p>
      <a class="btn btn-primary btn-sm" href="/pertanyaan/{{$tanyaan->id}}/edit">Edit Pertanyaan</a>
    </div>
  </div>


@endsection