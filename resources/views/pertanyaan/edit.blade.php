@extends('adminlte.master')

@section('content')
  <div class="ml-3 mt-3 mr-3">
   <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Pertanyaan #{{$tanyaan->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$tanyaan->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Judul</label>
          <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $tanyaan->judul)}}" placeholder="Masukkan Judul">
          @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Isi Pertanyaan</label>
          <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', $tanyaan->isi)}}" placeholder="Isi">
          @error('isi')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div></div>
        </form>
    </div>
   </div>

@endsection