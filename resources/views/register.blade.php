<!DOCTYPE html>
<html>
<head>
    <title>Buat Account Baru!</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    
    <form action="/welcome" method="POST">
    @csrf
    <fieldset>
        <!-- Nama 'text' -->
        <label for="nama1">First name:</label><br><br>
            <input type="text" name="firstname">
        <br><br>
        <label for="nama2">Last name:</label><br><br>
            <input type="text" name="lastname">
        <br><br>
        <!-- Gender 'radio' -->
        <label>Gender:</label><br><br>
            <input type="radio" name="gender" value="0">Male<br>
            <input type="radio" name="gender" value="1">Female<br>
            <input type="radio" name="gender" value="2">Other
        <br><br>
        <!-- Nationality 'option' -->
        <label>Nationality:</label><br><br>
        <select>
            <option value="0">Indonesian</option>
            <option value="1">Zimbabwe</option>
            <option value="2">Pluto</option> 
            <option value="3">Bekasi</option>
        </select>
        <br><br>
        <!-- Language 'checkbox' -->
        <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="language" value="0">Bahasa Indonesia<br>
            <input type="checkbox" name="language" value="1">English<br>
            <input type="checkbox" name="language" value="2">Other
        <br><br>
        <!-- Bio 'textarea' -->
        <label for="bio">Bio:</label><br><br>
            <textarea cols="40" rows="7" id="bio"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
    </fieldset>
    </form>

    <!-- By Aldi Satria Budi-->
</body>

</html>