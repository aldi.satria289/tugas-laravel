<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use Illuminate\Support\Facades\DB as FacadesDB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }
    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        
        // $query = DB::table('pertanyaan') // Query Builder
        //           ->insert([
        //             'judul' => $request["judul"],
        //             'isi' => $request["isi"]
        //             ]);

        // $pertanyaan = new Pertanyaan; //Eloquent atau ORM
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save(); // insert into pertanyaan(judul)

        $pertanyaan = Pertanyaan::create([  // Mass Assignment
                      "judul" => $request["judul"],
                      "isi"   => $request["isi"]
                      ]); 
   
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dibuat!');
    }
    public function index() {
        // $pertanyaan = DB::table('pertanyaan') // Query Builder
        //                  ->get();
        // dd($pertanyaan); 
        $pertanyaan = Pertanyaan::all(); // Eloquent
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function show($pertanyaan_id) {
        // $tanyaan = DB::table('pertanyaan') // Query Builder
        //              ->where('id', $pertanyaan_id)
        //             ->first();
        // dd($tanyaan); 
        $tanyaan = Pertanyaan::find($pertanyaan_id);
        return view('pertanyaan.show', compact('tanyaan'));
    }
    public function edit($pertanyaan_id) {
        // $tanyaan = DB::table('pertanyaan')
        //              ->where('id', $pertanyaan_id)
        //              ->first();
        // dd($tanyaan); 
        $tanyaan = Pertanyaan::find($pertanyaan_id);
        return view('pertanyaan.edit', compact('tanyaan'));
    }
    public function update($pertanyaan_id, Request $request) {
        
        // $query = DB::table('pertanyaan')
        //              ->where('id', $pertanyaan_id)
        //              ->update([
        //                 'judul' => $request['judul'],
        //                 'isi' => $request['isi']  
        //              ]);
        // dd($tanyaan); 
        $update = Pertanyaan::where('id', $pertanyaan_id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Diubah!');
    }
    public function destroy($pertanyaan_id) {
        // $query = DB::table('pertanyaan')
        //              ->where('id', $pertanyaan_id)
        //              ->delete();
        // dd($tanyaan); 
        Pertanyaan::destroy($pertanyaan_id);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus!');
    }
    
}
