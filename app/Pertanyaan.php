<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";

    // protected $fillable = ["judul","isi"]; // Untuk mass assignment
    protected $guarded = []; // kebalikan dari fillable (yang tidak dimasukkan)
}
